jQuery.fn.extend({  
    multiParsleyValidation = function(wform) {

    	var pane = jQuery(this);
		var form = $(wform);
		var steps = $('.steps li').size();
		var next = $('#wnext');
		var prev = $('#wprev');

		form.parsley( 'addListener', {
			    onFieldValidate: function ( elem ) {
			        if ( !$( elem ).is( ':visible' ) ) {
			            return true;
			        }
			        return false;
			    }
			});

		function changeButtom () {
			var current_step = pane.wizard('selectedItem').step;
			if(current_step == 1) {
				prev.hide();
			} else {
				prev.show();
			}
			if (steps == current_step){
				next.text('Enviar').removeClass('btn-white').addClass('btn-info');
			} else {
				next.text('Siguiente').removeClass('btn-info').addClass('btn-white');;
			}
		}

		next.on('click', function(event) { if (form.parsley('validate')) { pane.wizard('next'); }});
		prev.on('click', function(event) { pane.wizard('previous'); });

		pane.on('changed', function(e, data) {changeButtom();});
		pane.on('stepclick', function(e, data) {changeButtom();});
		pane.on('finished', function(e, data) { if (form.parsley('validate')) { form.submit(); }});
    }  
});