#multiParsleyValidation.js

Plugin para Parsley (Version 1.1.18) y Componente Wizard de Fuel UX (http://exacttarget.github.io/fuelux/#wizard)

##Uso

Primero Fuel UX dice que la sintaxis de su Wizard debe ser así. Pero al poner esto dentro de una etiqueta <form> y decirle a Parsley.js que la valide, esta solo analizara y someterá a validación solo el primer campo encontrado.

```html
<div class="panel">
	<div class="wizard clearfix" id="form-wizard">
		<ul class="steps">
			<li data-target="#step1" class="active">
				<span class="badge badge-info">1</span>
				Step 1
			</li>
			<li data-target="#step2">
				<span class="badge">2</span>
				Step 2
			</li>
			<li data-target="#step3">
				<span class="badge">3</span>
				Step 3
			</li>
		</ul>
	</div>
	<div class="step-content">
		<form id="parsley-form">
			<div class="step-pane active" id="step1">
				<p>Your website:</p>
				<input type="text" class="form-control" data-trigger="change" data-required="true" data-type="url"></div>
				<p>Your name:</p>
				<input type="text" class="form-control" data-trigger="change" data-required="true"></div>
				<p>Your Phone:</p>
				<input type="text" class="form-control" data-trigger="change" data-required="true"></div>
			<div class="step-pane" id="step2">
				<p>Your email:</p>
				<input type="text" class="form-control" data-trigger="change" data-required="true" data-type="email" placeholder="email address"></div>
				<p>Other email more:</p>
				<input type="text" class="form-control" data-trigger="change" data-required="true" data-type="email" placeholder="email address"></div>
			<div class="step-pane" id="step3">This is step 3</div>
		</form>
		<div class="actions m-t">
			<button type="button" class="btn btn-mini btn-prev"> <i class="icon-arrow-left"></i>Prev</button>
			<button type="button" class="btn btn-mini btn-next" data-last="Finish">Next<i class="icon-arrow-right"></i></button>
		</div>
	</div>
</div>
```

Para validar todo los cambios debes usar y sobre escribir los botones dentro de el div.actions y ponerlos fuera del <form>

```javascript 
$('#form-wizard').multiParsleyValidation('#parsley-form');
```

```html
<button id="wprev" class="btn btn-white btn-sm" style="display: none;">Atras</button>
<button id="wnext" class="btn btn-primary btn-sm">Siguiente</button>
```


